package pl.edu.agh.link.datamining.editor.preview.filters;

import static org.junit.Assert.assertEquals;
import static org.easymock.EasyMock.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;


public class CardinalityFilterTest {

	private CardinalityFilter filter;
	private Collection<IFrequentPattern> patterns;

	private static IFrequentPattern mock1;
	private static IFrequentPattern mock2;
	private static IFrequentPattern mock3;

	@BeforeClass
	public static void setClass(){
		mock1 = createMock(IFrequentPattern.class);
		mock2 = createMock(IFrequentPattern.class);
		mock3 = createMock(IFrequentPattern.class);

		expect(mock1.size()).andStubReturn(5);
		expect(mock2.size()).andStubReturn(10);
		expect(mock3.size()).andStubReturn(15);

		replay(mock1);
		replay(mock2);
		replay(mock3);
	}

	@Before
	public void setUp(){
		patterns = new LinkedList<>();

		patterns.add(mock1);
		patterns.add(mock2);
		patterns.add(mock3);
	}

	@Test
	public void cardinalityNothingFilteredCase() {
		filter = new CardinalityFilter(0, 20);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(patterns.size(), filteredPatterns.size());
	}

	@Test
	public void cardinalityEverythingFilteredCase(){
		filter = new CardinalityFilter(16, 20);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 0);
	}

	@Test
	public void cardinalityFirstValueFiltered(){
		filter = new CardinalityFilter(6, 20);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 2);
	}

	@Test
	public void cardinalityLastValueFiltered(){
		filter = new CardinalityFilter(4, 14);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 2);
	}

	@Test
	public void cardinalityBoundariesAreInclusive(){
		filter = new CardinalityFilter(5, 15);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 3);
	}
}
