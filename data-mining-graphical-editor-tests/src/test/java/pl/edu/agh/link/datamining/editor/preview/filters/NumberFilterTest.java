package pl.edu.agh.link.datamining.editor.preview.filters;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.cast.data.model.IElement;
import pl.edu.agh.link.billing.model.IPhoneCall;
import pl.edu.agh.link.billing.model.IPhoneNumber;
import pl.edu.agh.link.billing.model.PhoneCall;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternItem;

public class NumberFilterTest {

	private NumberFilter filter;
	private Collection<IFrequentPattern> patterns;

	private static IFrequentPattern patternMock;
	private static IFrequentPatternItem patternItemMock;
	private static IPhoneCall phoneCallMock;
	private static IPhoneNumber phoneNumberMock1;
	private static IPhoneNumber phoneNumberMock2;
	private static LinkedList<IFrequentPatternItem> list1 = new LinkedList<>();
	private static LinkedList<IPhoneCall> list2 = new LinkedList<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		patternMock = createMock(IFrequentPattern.class);
		patternItemMock = createMock(IFrequentPatternItem.class);
		phoneCallMock = createMock(IPhoneCall.class);
		phoneNumberMock1 = createMock(IPhoneNumber.class);
		phoneNumberMock2 = createMock(IPhoneNumber.class);

		list1.add(patternItemMock);
		list2.add(phoneCallMock);


		expect(patternMock.getItemList()).andStubReturn(list1);
		expect((LinkedList<IPhoneCall>)(patternItemMock.getOriginalElements())).andStubReturn(list2);
		expect(phoneCallMock.getSourceNumber()).andStubReturn(phoneNumberMock1);
		expect(phoneCallMock.getTargetNumber()).andStubReturn(phoneNumberMock2);
		expect(phoneNumberMock1.getName()).andStubReturn("123456");
		expect(phoneNumberMock2.getName()).andStubReturn("7654321");

		replay(patternMock);
		replay(patternItemMock);
		replay(phoneCallMock);
		replay(phoneNumberMock1);
		replay(phoneNumberMock2);
	}

	@Before
	public void setUp() throws Exception {
		patterns = new LinkedList<>();

		patterns.add(patternMock);
	}

	@Test
	public void numberContainsTest() {
		filter = new NumberFilter("123");
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 1);
	}

	@Test
	public void numberNotFoundTest(){
		filter = new NumberFilter("1236");
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 0);
	}

	@Test
	public void wholeNumberTest(){
		filter = new NumberFilter("123456");
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 1);
	}

}
