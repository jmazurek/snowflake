package pl.edu.agh.link.datamining.editor.preview.filters;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class SupportFilterTest {
	private SupportFilter filter;
	private Collection<IFrequentPattern> patterns;

	private static IFrequentPattern mock1;
	private static IFrequentPattern mock2;
	private static IFrequentPattern mock3;

	@BeforeClass
	public static void setClass(){
		mock1 = createMock(IFrequentPattern.class);
		mock2 = createMock(IFrequentPattern.class);
		mock3 = createMock(IFrequentPattern.class);

		expect(mock1.getAbsSupport()).andStubReturn(5);
		expect(mock2.getAbsSupport()).andStubReturn(10);
		expect(mock3.getAbsSupport()).andStubReturn(15);

		replay(mock1);
		replay(mock2);
		replay(mock3);
	}

	@Before
	public void setUp(){
		patterns = new LinkedList<>();

		patterns.add(mock1);
		patterns.add(mock2);
		patterns.add(mock3);
	}

	@Test
	public void supportNothingFilteredCase() {
		filter = new SupportFilter(0, 20);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(patterns.size(), filteredPatterns.size());
	}

	@Test
	public void supportEverythingFilteredCase(){
		filter = new SupportFilter(16, 20);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 0);
	}

	@Test
	public void supportFirstValueFiltered(){
		filter = new SupportFilter(6, 20);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 2);
	}

	@Test
	public void supportLastValueFiltered(){
		filter = new SupportFilter(4, 14);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 2);
	}

	@Test
	public void supportBoundariesAreInclusive(){
		filter = new SupportFilter(5, 15);
		Collection<IFrequentPattern> filteredPatterns = filter.filter(patterns);
		assertEquals(filteredPatterns.size(), 3);
	}


}
