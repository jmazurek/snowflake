package pl.edu.agh.link.datamining.schema.frequentpatterns;

import pl.edu.agh.cast.data.operator.AbstractOperatorFactory;
import pl.edu.agh.cast.data.operator.IOperator;
import pl.edu.agh.cast.data.operator.OperatorSpecification;
import pl.edu.agh.cast.data.operator.Parameter;
import pl.edu.agh.cast.schema.model.presentation.ISchemaDataSet;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternsDataSet;

public class FrequentPatterns2SchemaConverterSpecification extends AbstractOperatorFactory {

    private static final OperatorSpecification SPECIFICATION = new OperatorSpecification(new Parameter(IFrequentPatternsDataSet.class),
            new Parameter(ISchemaDataSet.class));

    /**
     * @param specification
     */
    public FrequentPatterns2SchemaConverterSpecification() {
        super(SPECIFICATION);
    }

    @Override
    public IOperator create() {
        return new FrequentPatterns2SchemaConverter(SPECIFICATION);
    }

}
