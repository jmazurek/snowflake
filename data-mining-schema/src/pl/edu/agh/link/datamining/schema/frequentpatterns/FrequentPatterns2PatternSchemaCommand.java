/*
 * This file is a part of CAST project.
 * (c) Copyright 2007, AGH University of Science & Technology
 * https://caribou.iisg.agh.edu.pl/trac/cast
 *
 * Licensed under the Eclipse Public License, Version 1.0 (the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.eclipse.org/legal/epl-v10.html
 */
/*
 * File: FrequentPatterns2PatternSchemaCommand.java
 * Created: 2014-11-13
 * Author: CAST-user
 * $Id$
 */

package pl.edu.agh.link.datamining.schema.frequentpatterns;

import pl.edu.agh.cast.data.operator.IOperatorFactory;
import pl.edu.agh.cast.schema.editor.layout.command.AbstractLayoutOperatorCommand;

/**
 *
 * @author AGH CAST Team
 */
public class FrequentPatterns2PatternSchemaCommand extends AbstractLayoutOperatorCommand<FrequentPatterns2PatternSchemaConverter>{

    public FrequentPatterns2PatternSchemaCommand(IOperatorFactory factory) {
        super(new FrequentPatterns2PatternSchemaConverterSpecification());
    }

}
