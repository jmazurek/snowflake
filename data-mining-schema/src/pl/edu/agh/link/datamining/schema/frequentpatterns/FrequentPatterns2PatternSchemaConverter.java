/*
 * This file is a part of CAST project.
 * (c) Copyright 2007, AGH University of Science & Technology
 * https://caribou.iisg.agh.edu.pl/trac/cast
 *
 * Licensed under the Eclipse Public License, Version 1.0 (the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.eclipse.org/legal/epl-v10.html
 */
/*
 * File: FrequentPatterns2PatternSchemaConverter.java
 * Created: 2014-11-13
 * Author: CAST-user
 * $Id$
 */

package pl.edu.agh.link.datamining.schema.frequentpatterns;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.cast.common.IProgressStatusMonitor;
import pl.edu.agh.cast.data.operator.AbstractUnaryOperator;
import pl.edu.agh.cast.data.operator.OperatorException;
import pl.edu.agh.cast.data.operator.OperatorSpecification;
import pl.edu.agh.cast.schema.model.presentation.ISchemaDataSet;
import pl.edu.agh.cast.schema.model.presentation.SchemaConnection;
import pl.edu.agh.cast.schema.model.presentation.SchemaDataSet;
import pl.edu.agh.cast.schema.model.presentation.SchemaNode;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternItem;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternsDataSet;

public class FrequentPatterns2PatternSchemaConverter extends AbstractUnaryOperator<IFrequentPatternsDataSet, ISchemaDataSet>{

    public static final String PATTERN_SVG_ID = "pl.edu.agh.link.bts.model.IBTS";
    public static final String NUMBER_SVG_ID = "pl.edu.agh.link.billing.model.IPhoneNumber";


    public FrequentPatterns2PatternSchemaConverter(OperatorSpecification specification) {
        super(specification);
    }

    @Override
    protected ISchemaDataSet doPerform(IFrequentPatternsDataSet fpds, IProgressStatusMonitor monitor) throws OperatorException {
        ISchemaDataSet sds = new SchemaDataSet(fpds.getName());
        int index = 0;
        sds.setOriginalDataSets(Collections.singleton(fpds));
        for(IFrequentPattern pattern : fpds.getFrequentPatterns()){
            SchemaNode patternNode = new SchemaNode("w" + index++);
            patternNode.setResourceId(PATTERN_SVG_ID);
            sds.addElement(patternNode);
            for(IFrequentPatternItem item : pattern.getItemList()){
                SchemaNode itemNode = new SchemaNode(item.getValue());
                itemNode.setResourceId(NUMBER_SVG_ID);
                SchemaConnection connection = new SchemaConnection(patternNode, itemNode);
                sds.addElement(itemNode);
                sds.addElement(connection);
            }
        }
        return sds;
    }

}
