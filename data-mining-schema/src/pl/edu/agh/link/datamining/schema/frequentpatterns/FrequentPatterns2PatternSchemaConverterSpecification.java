/*
 * This file is a part of CAST project.
 * (c) Copyright 2007, AGH University of Science & Technology
 * https://caribou.iisg.agh.edu.pl/trac/cast
 *
 * Licensed under the Eclipse Public License, Version 1.0 (the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.eclipse.org/legal/epl-v10.html
 */
/*
 * File: FrequentPatterns2PatternSchemaConverterSpecification.java
 * Created: 2014-11-13
 * Author: CAST-user
 * $Id$
 */

package pl.edu.agh.link.datamining.schema.frequentpatterns;

import pl.edu.agh.cast.data.operator.AbstractOperatorFactory;
import pl.edu.agh.cast.data.operator.IOperator;
import pl.edu.agh.cast.data.operator.OperatorSpecification;
import pl.edu.agh.cast.data.operator.Parameter;
import pl.edu.agh.cast.schema.model.presentation.ISchemaDataSet;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternsDataSet;

/**
 *
 * @author AGH CAST Team
 */
public class FrequentPatterns2PatternSchemaConverterSpecification extends AbstractOperatorFactory{

    private static final OperatorSpecification SPECIFICATION = new OperatorSpecification(new Parameter(IFrequentPatternsDataSet.class),
            new Parameter(ISchemaDataSet.class));

    public FrequentPatterns2PatternSchemaConverterSpecification() {
        super(SPECIFICATION);
    }

    @Override
    public IOperator create() {
        return new FrequentPatterns2PatternSchemaConverter(SPECIFICATION);
    }

}
