package pl.edu.agh.link.datamining.schema.frequentpatterns;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.edu.agh.cast.common.IProgressStatusMonitor;
import pl.edu.agh.cast.common.collections.Pair;
import pl.edu.agh.cast.data.operator.AbstractUnaryOperator;
import pl.edu.agh.cast.data.operator.OperatorException;
import pl.edu.agh.cast.data.operator.OperatorSpecification;
import pl.edu.agh.cast.schema.model.presentation.ISchemaDataSet;
import pl.edu.agh.cast.schema.model.presentation.SchemaConnection;
import pl.edu.agh.cast.schema.model.presentation.SchemaDataSet;
import pl.edu.agh.cast.schema.model.presentation.SchemaNode;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternItem;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternsDataSet;

public class FrequentPatterns2SchemaConverter extends AbstractUnaryOperator<IFrequentPatternsDataSet, ISchemaDataSet> {

    public static final String NUMBER_SVG_ID = "pl.edu.agh.link.billing.model.IPhoneNumber";

    public FrequentPatterns2SchemaConverter(OperatorSpecification specification) {
        super(specification);
    }

    @Override
    protected ISchemaDataSet doPerform(IFrequentPatternsDataSet fpds, IProgressStatusMonitor monitor) throws OperatorException {
        ISchemaDataSet sds = new SchemaDataSet(fpds.getName());

        sds.setOriginalDataSets(Collections.singleton(fpds));
        Map<String, SchemaNode> nodeMap = new HashMap<String, SchemaNode>();
        for (IFrequentPattern frequentPattern : fpds.getFrequentPatterns()) {
            for (IFrequentPatternItem item : frequentPattern.getItemList()) {
                Pair<String, String> numbers = extractNumbersFromValue(item.getValue());
                if (!nodeMap.containsKey(numbers.getFirst())) {
                    SchemaNode node = new SchemaNode(numbers.getFirst());
                    nodeMap.put(numbers.getFirst(), node);
                    node.setResourceId(NUMBER_SVG_ID);
                    sds.addElement(node);
                }
                if (!nodeMap.containsKey(numbers.getSecond())) {
                    SchemaNode node = new SchemaNode(numbers.getSecond());
                    nodeMap.put(numbers.getSecond(), node);
                    node.setResourceId(NUMBER_SVG_ID);
                    sds.addElement(node);
                }
                SchemaConnection connection = new SchemaConnection(nodeMap.get(numbers.getFirst()), nodeMap.get(numbers.getSecond()));
                sds.addElement(connection);
            }
        }

        return sds;
    }

    private Pair<String, String> extractNumbersFromValue(String connection) {
        Pattern p = Pattern.compile("(brak nr|\\d+) -> (brak nr|\\d+)");
        Matcher m = p.matcher(connection);
        m.find();
        String[] splited = m.group().split("->");
        return Pair.create(splited[0].trim(), splited[1].trim());
    }

}
