package pl.edu.agh.link.datamining.model.frequentpatterns;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import pl.edu.agh.cast.common.StringConstants;
import pl.edu.agh.cast.data.model.IElement;
import pl.edu.agh.cast.data.model.presentation.PresentationElement;

import pl.edu.agh.link.datamining.model.transactions.ITransaction;

/**
 * Represents a frequent pattern.
 */
public class FrequentPattern extends PresentationElement<IElement> implements IFrequentPattern {

    private static final long serialVersionUID = -569122580567412670L;

    private List<IFrequentPatternItem> itemList;

    private Set<Integer> suppTransIDset = new HashSet<>();

    private Integer absSupport;

    private Double support;

    /**
     * Instantiates a new frequent pattern.
     */
    public FrequentPattern() { }

    /**
     * Instantiates a new frequent pattern.
     *
     * @param itemList
     *            the item list
     */
    public FrequentPattern(List<IFrequentPatternItem> itemList) {
        this.itemList = itemList;
    }

    @Override
    public Set<Integer> getSupportingTransactionIDset() {
        return suppTransIDset;
    }

    /**
     * Sets the set of Id of supporting transactions.
     *
     * @param suppTransIDs
     *            the new set of Id of supporting transactions
     */
    public void setSupportingTransactionIDset(Set<Integer> suppTransIDs) {
        this.suppTransIDset = suppTransIDs;
    }

    /**
     * Adds the supporting transaction.
     *
     * @param transaction
     *            the transaction
     */
    public void addSupportingTransaction(ITransaction transaction) {
        suppTransIDset.add(transaction.getTransID());
        absSupport = suppTransIDset.size();
    }

    @Override
    public List<IFrequentPatternItem> getItemList() {
        return itemList;
    }

    @Override
    public void setItemList(List<IFrequentPatternItem> itemList) {
        this.itemList = itemList;
    }

    @Override
    public Double getSupport() {
        return support;
    }

    /**
     * Gets the support for transaction count.
     *
     * @param transactionCount
     *            the transaction count
     * @return the support for transaction count
     */
    public Double getSupportForTransactionCount(int transactionCount) {
        return 1.0 * getAbsSupport() / transactionCount;
    }

    @Override
    public void setSupport(Double support) {
        this.support = support;
    }

    @Override
    public Integer getAbsSupport() {
        return absSupport;
    }

    @Override
    public void setAbsSupport(Integer absSupport) {
        this.absSupport = absSupport;
    }

    @Override
    public int size() {
        return itemList.size();
    }

    @Override
    public IFrequentPatternItem getLast() {
        return itemList.size() > 0 ? itemList.get(itemList.size() - 1) : null;
    }

    @Override
    public void addTransIntersection(IFrequentPattern p, IFrequentPattern q) {
        if (p.getAbsSupport() < q.getAbsSupport()) {
            for (Integer trID : p.getSupportingTransactionIDset()) {
                if (q.getSupportingTransactionIDset().contains(trID)) {
                    this.suppTransIDset.add(trID);
                }
            }
        } else {
            for (Integer trID : q.getSupportingTransactionIDset()) {
                if (p.getSupportingTransactionIDset().contains(trID)) {
                    this.suppTransIDset.add(trID);
                }
            }
        }
        this.absSupport = this.suppTransIDset.size();
    }

    @Override
    public void calculateSupportForTransactionCount(int transactionCount) {
        this.support = 1.0 * absSupport / transactionCount;
    }

    @Override
    public String getValue() {
        StringBuilder sb = new StringBuilder();
        for (IFrequentPatternItem patternItem : itemList) {
            sb.append(patternItem.getValue() + StringConstants.SEMICOLON);
        }
        return sb.toString();
    }

    @Override
    public void setValue(String value) {

    }

    @Override
    public String toString() {
        return itemList.toString();
    }

    @Override
    public IFrequentPattern subtract(IFrequentPattern pattern) {
        FrequentPattern resultPattern = new FrequentPattern();

        List<Integer> hashItems1 = new LinkedList<>();
        List<Integer> hashItems2 = new LinkedList<>();

        for (IFrequentPatternItem item : getItemList()) {
            hashItems1.add(item.getHashValue());
        }

        for (IFrequentPatternItem item : pattern.getItemList()) {
            hashItems2.add(item.getHashValue());
        }

        hashItems1.removeAll(hashItems2);

        List<IFrequentPatternItem> result = new LinkedList<>();

        for (IFrequentPatternItem item : getItemList()) {
            if (hashItems1.contains(item.getHashValue())) {
                result.add(item);
            }
        }

        resultPattern.setItemList(result);

        return resultPattern;
    }

    /**
     * For MultiTree.
     * @return incremental hash
     */
    public Integer specialHashCode() {
        int magicPrime = 2147483629;
        int hash = 0;
        for(int i = 0; i < itemList.size(); i++) {
            int element = itemList.get(i).hashCode() % magicPrime;
            while(element < 0) {
                element += magicPrime;
                element %= magicPrime;
            }
//            System.out.println("item: "+itemList.get(i)+ " hashcode: " + element);
            hash += element;
            hash %= magicPrime;
            if(hash < 0) {
                hash -= Integer.MIN_VALUE;
                hash %= magicPrime;
            }
        }
//        System.out.println("pattern: "+this+ " hash: " +hash);
        return hash;
    }

}
