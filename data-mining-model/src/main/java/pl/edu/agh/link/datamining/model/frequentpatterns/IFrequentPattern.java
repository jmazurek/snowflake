package pl.edu.agh.link.datamining.model.frequentpatterns;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import pl.edu.agh.cast.data.model.metadata.annotations.ModelType;
import pl.edu.agh.cast.data.model.metadata.annotations.ReferenceList;
import pl.edu.agh.cast.data.model.metadata.annotations.Value;
import pl.edu.agh.cast.data.model.property.AbstractPropertyLocalizationProvider;
import pl.edu.agh.cast.data.model.property.annotation.Localization;
import pl.edu.agh.cast.data.model.property.annotation.PermanentProperty;
import pl.edu.agh.cast.data.model.property.annotation.type.DoublePropertyType;
import pl.edu.agh.cast.data.model.property.annotation.type.IntegerPropertyType;
import pl.edu.agh.cast.data.model.property.annotation.type.ReferenceCollectionPropertyType;
import pl.edu.agh.cast.data.model.property.annotation.type.TextPropertyType;

import pl.edu.agh.link.datamining.model.IDMElement;
import pl.edu.agh.link.datamining.model.util.Messages;

/**
 * Represents a frequent pattern.
 */
@ModelType("ct:/datamining/default/frequent-pattern")
@Localization(IFrequentPattern.LocalizationProvider.class)
public interface IFrequentPattern extends IDMElement {

    /**
     * The Class Properties.
     */
    public static class Properties {

        /** The Constant ITEM_LIST. */
        public static final String ITEM_LIST = "ITEM_LIST"; //$NON-NLS-1$

        /** Value of support. */
        public static final String SUPPORT = "SUPPORT"; //$NON-NLS-1$

        /** Value of absolute support. */
        public static final String ABS_SUPPORT = "ABS_SUPPORT"; //$NON-NLS-1$

        /** The Constant VALUE. */
        public static final String VALUE = "VALUE"; //$NON-NLS-1$
    }

    /**
     * Localization provider.
     */
    public class LocalizationProvider extends AbstractPropertyLocalizationProvider {
        @Override
        protected void init() {
            addDisplayName(Properties.ITEM_LIST, Messages.IFrequentPattern_ItemList);
            addDisplayName(Properties.SUPPORT, Messages.IFrequentPattern_Support);
            addDisplayName(Properties.ABS_SUPPORT, Messages.IFrequentPattern_AbsoluteSupport);
            addDisplayName(Properties.VALUE, Messages.IFrequentPattern_Value);
        }
    }

    /**
     * Gets the item list.
     *
     * @return the item list
     */
    @PermanentProperty(name = Properties.ITEM_LIST)
    @ReferenceCollectionPropertyType
    @ReferenceList(collectionType = LinkedList.class)
    List<IFrequentPatternItem> getItemList();

    /**
     * Sets the item list.
     *
     * @param itemList
     *            the new item list
     */
    void setItemList(List<IFrequentPatternItem> itemList);

    /**
     * Gets the set of Id of supporting transactions.
     *
     * @return the set of Id of supporting transactions
     */
    Set<Integer> getSupportingTransactionIDset();

    /**
     * Gets the support.
     *
     * @return the support
     */
    @PermanentProperty(name = Properties.SUPPORT)
    @DoublePropertyType
    @Value
    Double getSupport();

    /**
     * Sets the support.
     *
     * @param support
     *            the new support
     */
    public void setSupport(Double support);

    /**
     * Gets the absolute support.
     *
     * @return the absolute support
     */
    @PermanentProperty(name = Properties.ABS_SUPPORT)
    @IntegerPropertyType
    @Value
    Integer getAbsSupport();

    /**
     * Sets the abs support.
     *
     * @param support
     *            the new abs support
     */
    void setAbsSupport(Integer support);

    /**
     * Calculate support for transaction count.
     *
     * @param transactionCount
     *            the transaction count
     */
    void calculateSupportForTransactionCount(int transactionCount);

    /**
     * Gets the value.
     *
     * @return the value
     */
    @PermanentProperty(name = Properties.VALUE)
    @TextPropertyType
    @Value
    String getValue();

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    void setValue(String value);

    /**
     * Number of items in the frequent pattern.
     *
     * @return the number of items in the frequent pattern
     */
    int size();

    /**
     * Subtract.
     *
     * @param pattern
     *            the pattern
     * @return the frequent pattern
     */
    IFrequentPattern subtract(IFrequentPattern pattern);

    /**
     * Gets the last item in the frequent pattern.
     *
     * @return the last item
     */
    IFrequentPatternItem getLast();

    /**
     * Adds the transaction intersection to frequent pattern as supporting transactions.
     *
     * @param p
     *            the first frequent pattern
     * @param q
     *            the second one
     */
    void addTransIntersection(IFrequentPattern p, IFrequentPattern q);

    Integer specialHashCode();

}
