package pl.edu.agh.link.datamining.model.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.cast.common.collections.Pair;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

/**
 * Each pattern may have mutliple references, not a synchronized collection.
 */
public class FrequentPatternMultiTree {

    private List<IFrequentPattern> flatView;
    private Map<Integer, IFrequentPattern> indexedPatterns;
    private Map<Integer, Pair<Integer, Integer>> sizeGroupDelimeters; // many top-levels ?
    private int maxPatternSize;

    private void preprocess() {
        // auto removal of entries removed from flatView
        indexedPatterns =  new HashMap<>();
        sizeGroupDelimeters = new HashMap<>();
        maxPatternSize = flatView.get(0).size();
        int beginIndex = 0;
        int currentSize = maxPatternSize;
        for(int curr = 0; curr < flatView.size(); curr++) {
            if(flatView.get(curr).size() < currentSize) {
                sizeGroupDelimeters.put(currentSize, new Pair<>(beginIndex, curr));
                currentSize--;
                beginIndex = curr;
            }
            IFrequentPattern pattern = flatView.get(curr);
            int hash = pattern.specialHashCode();
            indexedPatterns.put(hash, pattern);
            //System.out.println("Special index: " + hash + " for: "+pattern);
        }
        sizeGroupDelimeters.put(currentSize, new Pair<>(beginIndex, flatView.size()));
    }

    private void sortDecreasingOrder(Collection<IFrequentPattern> flatCollection) {
        if (flatCollection instanceof List) {
            flatView = (List<IFrequentPattern>) flatCollection;
        } else {
            flatView = new ArrayList<>(flatCollection);
        }
        Collections.sort(flatView, new Comparator<IFrequentPattern>() {
            @Override
            public int compare(IFrequentPattern arg0, IFrequentPattern arg1) {
                return arg1.size() - arg0.size();
            }
        });
    }

    public FrequentPatternMultiTree(Collection<IFrequentPattern> flatCollection) {
        sortDecreasingOrder(flatCollection);
        preprocess();
    }


    public List<IFrequentPattern> sortedDecreasingFlatView() {
        return Collections.unmodifiableList(flatView);
    }

    public List<FrequentPatternMultiTreeNode> rootNodes() {
        Pair<Integer, Integer> bounds = sizeGroupDelimeters.get(maxPatternSize);
        List<FrequentPatternMultiTreeNode> rootNodes = new ArrayList<>();
        for(IFrequentPattern pattern : flatView.subList(bounds.getFirst(), bounds.getSecond())) {
            rootNodes.add(new FrequentPatternMultiTreeNode(indexedPatterns, pattern));
        }
        return rootNodes;
    }


}
