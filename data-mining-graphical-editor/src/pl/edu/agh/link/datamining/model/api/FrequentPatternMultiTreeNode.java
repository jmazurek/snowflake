package pl.edu.agh.link.datamining.model.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class FrequentPatternMultiTreeNode {

    private Map<Integer, IFrequentPattern> indexedPatterns;
    private IFrequentPattern value;
    private List<FrequentPatternMultiTreeNode> children;

    public FrequentPatternMultiTreeNode(Map<Integer, IFrequentPattern> indexedPatterns, IFrequentPattern value) {
        this.indexedPatterns = indexedPatterns;
        this.value = value;
    }

    public IFrequentPattern getValue() { return value; }

    // Lazily generated
    public List<FrequentPatternMultiTreeNode> getChildren() {
        if(children == null) {
            children = new ArrayList<>();
            int magicPrime = 2147483629;
            int original = value.specialHashCode();
            for(int i = 0; i < value.getItemList().size(); i++) {
                int element = value.getItemList().get(i).hashCode() % magicPrime;
                while(element < 0) {
                    element += magicPrime;
                    element %= magicPrime;
                }
//                System.out.println("item removed: "+value.getItemList().get(i)+ " hashcode: " + element);
                int hash = original - element;
                hash %= magicPrime;
                if(hash < 0) {
                    hash -= Integer.MIN_VALUE;
                    hash %= magicPrime;
                }
//                System.out.println("Special index searched for: " + hash);
                if(indexedPatterns.containsKey(hash)) {
                    children.add(new FrequentPatternMultiTreeNode(indexedPatterns, indexedPatterns.get(hash)));
                }
            }
        }
        return children;
    }

}
