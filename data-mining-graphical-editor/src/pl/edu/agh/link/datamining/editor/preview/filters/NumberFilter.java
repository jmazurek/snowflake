package pl.edu.agh.link.datamining.editor.preview.filters;

import java.util.Collection;
import java.util.Iterator;

import pl.edu.agh.cast.data.model.IElement;

import pl.edu.agh.link.billing.model.IPhoneCall;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternItem;

public class NumberFilter implements IPatternFilter {

    private String number;

    public NumberFilter(String number) {
        this.number = number;
    }

    @Override
    public Collection<IFrequentPattern> filter(Collection<IFrequentPattern> patterns) {
        Iterator<IFrequentPattern> iterator = patterns.iterator();
        while (iterator.hasNext()) {
            boolean found = false;
            IFrequentPattern pattern = iterator.next();
            for (IFrequentPatternItem item : pattern.getItemList()) {
                for (IElement el : item.getOriginalElements()) {
                    IPhoneCall call = (IPhoneCall)el;
                    if (call.getSourceNumber().getName().contains(number) || call.getTargetNumber().getName().contains(number)) {
                        found = true;
                    }
                }
                if (found) {
                    break;
                }
            }
            if (!found) {
                iterator.remove();
            }
        }
        return patterns;
    }

}
