package pl.edu.agh.link.datamining.editor.details;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import pl.edu.agh.cast.ui.util.PartListenerAdapter;

import pl.edu.agh.link.datamining.editor.DataMiningEditor;
import pl.edu.agh.link.datamining.editor.figures.FrequentPatternFigureBuilder;
import pl.edu.agh.link.datamining.editor.figures.coloring.StaticColoringStrategy;
import pl.edu.agh.link.datamining.editor.figures.location.CenterLocationStrategy;
import pl.edu.agh.link.datamining.editor.figures.presentation.DetailedPatternStrategy;
import pl.edu.agh.link.datamining.editor.figures.presentation.IPatternPresentationStrategy;
import pl.edu.agh.link.datamining.editor.preview.PreviewEditorManager;
import pl.edu.agh.link.datamining.editor.util.Messages;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class DetailedView extends ViewPart {

    private PreviewEditorManager manager = null;

    private Figure workspace;

    private Canvas canvas;

    private LightweightSystem lightweightSystem;

    private IPatternPresentationStrategy patternStrategy = new DetailedPatternStrategy();

    private StaticColoringStrategy coloringStrategy = new StaticColoringStrategy();

    private CenterLocationStrategy locationStrategy;

    @Override
    public void createPartControl(Composite parent) {
        initialize(parent);
    }

    private void initialize(Composite parent) {
        canvas = new Canvas(parent, SWT.NONE);
        workspace = new Figure();
        locationStrategy = new CenterLocationStrategy(workspace);
        lightweightSystem = new LightweightSystem(canvas);
        initializeManager();
    }

    @Override
    public void setFocus() {
        // TODO Auto-generated method stub

    }

    private void initializeManager() {
        IEditorPart editorPart = getSite().getPage().getActiveEditor();
        if (editorPart instanceof DataMiningEditor) {
            DataMiningEditor editor = (DataMiningEditor)editorPart;
            manager = editor.getPreviewEditorManager();
            manager.setDetailedView(DetailedView.this);
        }
        createPartListener();
    }

    private void updateContents() {
        IEditorPart editorPart = getSite().getPage().getActiveEditor();
        if (editorPart instanceof DataMiningEditor) {
            lightweightSystem.setContents(workspace);
        } else {
            lightweightSystem.setContents(new Label(Messages.DetailedView_NotSupportedDataType));
        }
    }

    private void createPartListener() {
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().addPartListener(new PartListenerAdapter() {
            @Override
            public void partOpened(IWorkbenchPart part) {
                if (part instanceof DataMiningEditor) {
                    DataMiningEditor editor = (DataMiningEditor)part;
                    manager = editor.getPreviewEditorManager();
                    manager.setDetailedView(DetailedView.this);
                }
                updateContents();
            }

            @Override
            public void partClosed(IWorkbenchPart part) {
                if (part instanceof DataMiningEditor) {
                    manager = null;
                    showPattern(null, null);
                } else if (part instanceof DetailedView) {
                    manager.setDetailedView(null);
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().removePartListener(this);
                }
                updateContents();
            }

            @Override
            public void partActivated(IWorkbenchPart part) {
                updateContents();
            }

        });
    }

    public void showPattern(IFrequentPattern pattern, Color color) {
        workspace.removeAll();

        if (pattern != null) {
            FrequentPatternFigureBuilder builder = new FrequentPatternFigureBuilder(patternStrategy, coloringStrategy, locationStrategy);
            coloringStrategy.setColor(color);
            IFigure fig = builder.build(pattern);
            workspace.add(fig);
        }
    }

}
