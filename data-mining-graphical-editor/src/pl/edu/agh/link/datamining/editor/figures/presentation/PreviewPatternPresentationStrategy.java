package pl.edu.agh.link.datamining.editor.figures.presentation;

import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Dimension;

import pl.edu.agh.cast.ui.util.ColorUtil;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class PreviewPatternPresentationStrategy implements
		IPatternPresentationStrategy {

	private final Dimension size;

	public PreviewPatternPresentationStrategy(int width, int heigth) {
		size = new Dimension(width, heigth);
	}

	@Override
	public Shape buildFigure(IFrequentPattern pattern) {
		final RectangleFigure rectangleFigure = new RectangleFigure();
        rectangleFigure.setSize(size.width, size.height-2);

        rectangleFigure.setForegroundColor(ColorUtil.getColor(0, 0, 0));
        rectangleFigure.setOutline(false);
        rectangleFigure.setAlpha(50);

        rectangleFigure.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseMoved(MouseEvent me) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseHover(MouseEvent me) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent me) {
                rectangleFigure.setAlpha(50);

            }

            @Override
            public void mouseEntered(MouseEvent me) {
                rectangleFigure.setAlpha(100);

            }

            @Override
            public void mouseDragged(MouseEvent me) {
                // TODO Auto-generated method stub

            }
        });

        return rectangleFigure;
	}

}
