package pl.edu.agh.link.datamining.editor.figures.location;

import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Point;

public interface IPatternLocationStrategy {
    Point computeLocation(Shape pattern);
}
