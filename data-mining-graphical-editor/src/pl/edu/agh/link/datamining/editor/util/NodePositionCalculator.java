package pl.edu.agh.link.datamining.editor.util;

import org.eclipse.draw2d.geometry.Point;

public class NodePositionCalculator {
    public static Point computePosition(int r, double alfa){
        alfa = Math.toRadians(alfa);
        double x = r*Math.sin(alfa) + r;
        double y = r*Math.cos(alfa) + r;

        return new Point((int)x, (int)y);
    }
}
