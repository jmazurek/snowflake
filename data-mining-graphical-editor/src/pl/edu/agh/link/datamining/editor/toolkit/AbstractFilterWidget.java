package pl.edu.agh.link.datamining.editor.toolkit;

import org.eclipse.swt.widgets.Composite;

import pl.edu.agh.link.datamining.editor.preview.filters.AbstractFilterFactory;

public abstract class AbstractFilterWidget extends Composite {

    public AbstractFilterWidget(Composite parent, int style) {
        super(parent, style);
    }

    public abstract AbstractFilterFactory getFilterFactory();
}
