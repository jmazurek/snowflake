package pl.edu.agh.link.datamining.editor.elements.view;

import java.util.List;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import pl.edu.agh.cast.common.StringConstants;
import pl.edu.agh.cast.ui.util.PartListenerAdapter;

import pl.edu.agh.link.datamining.editor.DataMiningEditor;
import pl.edu.agh.link.datamining.editor.figures.coloring.IPatternColoringStrategy;
import pl.edu.agh.link.datamining.editor.figures.coloring.SupportTemperatureColoringStrategy;
import pl.edu.agh.link.datamining.editor.util.Messages;
import pl.edu.agh.link.datamining.model.api.FrequentPatternMultiTree;
import pl.edu.agh.link.datamining.model.api.FrequentPatternMultiTreeNode;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class DataMiningElementsView extends ViewPart {

    private DataMiningEditor currentEditor = null;
    private Tree frequentPatternsTree;
    private Tree flatViewTree;
    private Menu rightClickMenu;
    private GridData patternsTreeData;
    private GridData flatTreeData;
    private IPatternColoringStrategy coloring;
    private FlatTreeSortingListener sortingListener;
    private String textSquare = "\u25A0"; // UTF-8 //$NON-NLS-1$

    private void configurePatternTree(Tree tree) {
        tree.setHeaderVisible(true);
        TreeColumn[] columns = new TreeColumn[5];
        for (int i = 0; i < 5; i++) {
            columns[i] = new TreeColumn(tree, SWT.NONE);
        }
        columns[0].setText(StringConstants.EMPTY);
        columns[1].setText(Messages.DataMiningElementsView_Name);
        columns[2].setText(Messages.DataMiningElementsView_Support);
        columns[3].setText(Messages.DataMiningElementsView_ElementCount);
        columns[4].setText(Messages.DataMiningElementsView_Color);
        for (TreeColumn col: columns) {
            col.pack();
            col.setMoveable(true);
        }
        columns[0].setMoveable(false);
        columns[0].setWidth(50);
    }

    @Override
    public void createPartControl(final Composite p) {

        final SashForm parent = new SashForm(p, SWT.HORIZONTAL);
        parent.setLayout(GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(2).margins(0, 0).spacing(0, 0).create());
        parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        registerDataListener();
        frequentPatternsTree = new Tree(parent, SWT.VIRTUAL | SWT.CHECK | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        flatViewTree = new Tree(parent, SWT.CHECK | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        configurePatternTree(frequentPatternsTree);
        configurePatternTree(flatViewTree);
        sortingListener = new FlatTreeSortingListener();
        for(TreeColumn col: flatViewTree.getColumns()) {
            col.addSelectionListener(sortingListener);
        }

        flatTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
        patternsTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
        flatViewTree.setLayoutData(flatTreeData);
        frequentPatternsTree.setLayoutData(patternsTreeData);

        frequentPatternsTree.setVisible(false);
        flatViewTree.setVisible(true);
        flatTreeData.exclude = false;
        patternsTreeData.exclude = true;

        /*
         * Listener listener = new Listener() {
         *
         * @Override public void handleEvent( Event e ) { final TreeItem treeItem = (TreeItem)e.item; p.getDisplay().asyncExec(new
         * Runnable() {
         *
         * @Override public void run() { for (TreeColumn tc : treeItem.getParent().getColumns()) { System.out.println(tc); tc.pack(); } }
         *
         * }); } }; flatViewTree.addListener(SWT.Collapse, listener); flatViewTree.addListener(SWT.Expand, listener);
         * frequentPatternsTree.addListener(SWT.Collapse, listener); frequentPatternsTree.addListener(SWT.Expand, listener);
         */

        rightClickMenu = new Menu(parent);
        flatViewTree.setMenu(rightClickMenu);
        frequentPatternsTree.setMenu(rightClickMenu);
        MenuItem tree = new MenuItem(rightClickMenu, SWT.NONE);
        tree.setText(Messages.DataMiningElementsView_TreeView);
        MenuItem flat = new MenuItem(rightClickMenu, SWT.NONE);
        flat.setText(Messages.DataMiningElementsView_FlatView);
        MenuItem flatParallel = new MenuItem(rightClickMenu, SWT.NONE);
        flatParallel.setText(Messages.DataMiningElementsView_Both);
        flat.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                frequentPatternsTree.setVisible(false);
                flatViewTree.setVisible(true);
                flatTreeData.exclude = false;
                patternsTreeData.exclude = true;

                parent.redraw();
                parent.layout();
            }
        });
        tree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                frequentPatternsTree.setVisible(true);
                flatViewTree.setVisible(false);
                flatTreeData.exclude = true;
                patternsTreeData.exclude = false;

                parent.redraw();
                parent.layout(false);

            }
        });
        flatParallel.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                frequentPatternsTree.setVisible(true);
                flatViewTree.setVisible(true);
                flatTreeData.exclude = false;
                patternsTreeData.exclude = false;

                frequentPatternsTree.pack();
                flatViewTree.pack();

                parent.redraw();
                parent.layout(false);
            }
        });

        /*
         * final Menu menu = new Menu(flatViewTree); flatViewTree.setMenu(menu); menu.addMenuListener(new MenuAdapter() { public void
         * menuShown(MenuEvent e) { MenuItem[] items = menu.getItems(); for (int i = 0; i < items.length; i++) { items[i].dispose(); }
         * MenuItem newItem = new MenuItem(menu, SWT.NONE); newItem.setText("Menu for " + flatViewTree.getSelection()[0].getText()); } });
         */


        getDataFromEditor();
    }

    private void registerDataListener() {
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().addPartListener(new PartListenerAdapter() {
            @Override
            public void partOpened(IWorkbenchPart part) {
                if (part instanceof DataMiningEditor) {
                    getDataFromEditor();
                }
            }

            @Override
            public void partClosed(IWorkbenchPart part) {
                if (part instanceof DataMiningEditor) {
                    getDataFromEditor();
                }
            }

            @Override
            public void partActivated(IWorkbenchPart part) {
                if (part instanceof DataMiningEditor) {
                    getDataFromEditor();
                }
            }
        });

    }

    private void getDataFromEditor() {
        IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
        if (page == null || !(page.getActiveEditor() instanceof DataMiningEditor)) {
            return;
        }
        frequentPatternsTree.removeAll();
        flatViewTree.removeAll();
        // ^ this is a temporary and ugly solution
        currentEditor = (DataMiningEditor)page.getActiveEditor();

        FrequentPatternMultiTree patterns = new FrequentPatternMultiTree(currentEditor.getFilteredPatterns());
        List<IFrequentPattern> list = patterns.sortedDecreasingFlatView();
        int maxSupport = 0, minSupport = Integer.MAX_VALUE;
        for(IFrequentPattern pattern : list) {
            if(pattern.getAbsSupport() > maxSupport)
                maxSupport = pattern.getAbsSupport();
            else if (pattern.getAbsSupport() < minSupport)
                minSupport = pattern.getAbsSupport();
        }

        coloring = new SupportTemperatureColoringStrategy(minSupport, maxSupport);
        for (IFrequentPattern pattern : patterns.sortedDecreasingFlatView()) {
            TreeItem entry = new TreeItem(flatViewTree, SWT.NONE);
            entry.setForeground(4, coloring.getColor(pattern));
            entry.setText(new String[] { StringConstants.EMPTY, pattern.getValue(), pattern.getAbsSupport().toString(),
                new Integer(pattern.getItemList().size()).toString(), textSquare });
            entry.setData(pattern);
        }

        for (FrequentPatternMultiTreeNode node : patterns.rootNodes()) {
            IFrequentPattern pattern = node.getValue();
            TreeItem entry = new TreeItem(frequentPatternsTree, SWT.NONE);
            entry.setForeground(4, coloring.getColor(pattern));
            entry.setText(new String[] { StringConstants.EMPTY, pattern.getValue(), pattern.getAbsSupport().toString(),
                new Integer(pattern.getItemList().size()).toString(), textSquare });
            entry.setData(node);

            new TreeItem(entry, SWT.NONE);
        }

        frequentPatternsTree.addListener(SWT.Expand, new Listener() {
            @Override
            public void handleEvent(final Event event) {
                final TreeItem root = (TreeItem)event.item;
                TreeItem[] items = root.getItems();
                List<FrequentPatternMultiTreeNode> children = ((FrequentPatternMultiTreeNode)root.getData()).getChildren();
                if (items.length < children.size()) {
                    root.removeAll();
                    for (FrequentPatternMultiTreeNode node : children) {
                        IFrequentPattern pattern = node.getValue();
                        TreeItem entry = new TreeItem(root, SWT.NONE);
                        entry.setForeground(4, coloring.getColor(pattern));
                        entry.setText(new String[] { StringConstants.EMPTY, pattern.getValue(), pattern.getAbsSupport().toString(),
                            new Integer(pattern.getItemList().size()).toString(), textSquare });
                        entry.setData(node);
                        if (pattern.size() > 1)
                            new TreeItem(entry, 0);
                    }
                }
            }
        });

        frequentPatternsTree.addListener(SWT.Selection, new Listener() {
            public void handleEvent(Event event) {
//                TreeItem item = ((TreeItem)event.item);
//                if (event.detail == SWT.CHECK) {
//                    if (item.getChecked())
//                        item.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GREEN));
//                    else
//                        item.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
//                }
            }
        });

        frequentPatternsTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDoubleClick(MouseEvent event) {

            }
        });
    }

    @Override
    public void setFocus() {}

}
