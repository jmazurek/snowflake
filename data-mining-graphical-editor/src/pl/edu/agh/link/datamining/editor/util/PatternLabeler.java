package pl.edu.agh.link.datamining.editor.util;

import pl.edu.agh.cast.data.model.IElement;

import pl.edu.agh.link.billing.model.PhoneCall;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternItem;

public class PatternLabeler {
    public static String createGroupLabel(IFrequentPattern pattern) {
        StringBuilder builder = new StringBuilder();
        builder.append(Messages.PatternLabeler_AbsoluteSupport).append(": ").append(pattern.getAbsSupport()); //$NON-NLS-1$
        for (IFrequentPatternItem item : pattern.getItemList()) {
            builder.append("\n\t"); //$NON-NLS-1$
            for (IElement el : item.getOriginalElements()) {
                PhoneCall call = (PhoneCall)el;
                builder.append(call.getSourceNumber()).append("==>").append(call.getTargetNumber()); //$NON-NLS-1$
            }
        }
        return builder.toString();
    }
}
