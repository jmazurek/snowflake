package pl.edu.agh.link.datamining.editor.preview;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

import pl.edu.agh.link.datamining.editor.DMEditor;
import pl.edu.agh.link.datamining.editor.figures.selection.ISelectionStrategy;
import pl.edu.agh.link.datamining.editor.figures.selection.MultiSelectionStrategy;
import pl.edu.agh.link.datamining.editor.preview.figures.PreviewPatternGroupFigureBuilder;
import pl.edu.agh.link.datamining.editor.util.Config;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class PreviewEditor extends DMEditor {
    private List<SizeListener> sizeListeners = new LinkedList<>();

    public PreviewEditor() {
        super();
    }

    @Override
    public void repaintPatterns() {
        editorSpace.removeAll();
        int currentPos = Config.PREVIEW_PATTERN_TOP_MARGIN;
        PreviewPatternGroupFigureBuilder builder = new PreviewPatternGroupFigureBuilder(this);
        int maxWidth = 0;
        for (Entry<Integer, List<IFrequentPattern>> entry : patterns.entrySet()) {
            IFigure fig = builder.withPatterns(entry.getValue())
                    .withSize(new Dimension(Config.PREVIEW_PATTERN_WIDTH, Config.PREVIEW_PATTERN_HEIGHT)).build();
            fig.setLocation(new Point(Config.PREVIEW_PATTERN_LEFT_MARGIN, currentPos));
            editorSpace.add(fig);
            currentPos += Config.PREVIEW_PATTERN_HEIGHT + Config.PREVIEW_PATTERN_Y_OFFSET;
            if (fig.getSize().width > maxWidth) {
                maxWidth = fig.getSize().width;
            }
        }
        invokeSizeListeners(new Point(maxWidth + Config.PREVIEW_PATTERN_RIGHT_MARGIN, currentPos + Config.PREVIEW_PATTERN_BOTTOM_MARGIN));
    }

    public void addSizeListener(SizeListener listener) {
        sizeListeners.add(listener);
    }

    public static interface SizeListener {
        public void sizeChanged(Point size);
    }

    private void invokeSizeListeners(Point size) {
        for (SizeListener listener : sizeListeners) {
            listener.sizeChanged(size);
        }
    }

    @Override
    protected ISelectionStrategy createSelectionStrategy() {
        return new MultiSelectionStrategy();
    }

}
