package pl.edu.agh.link.datamining.editor.preview.filters;

public class CardinalityFilterFactory extends AbstractFilterFactory {

    private int minCardinality = 1;

    private int maxCardinality = 1;

    public void setMaxCardinality(int maxCardinality) {
        this.maxCardinality = maxCardinality;
    }

    public void setMinCardinality(int minCardinality) {
        this.minCardinality = minCardinality;
    }

    @Override
    protected IPatternFilter buildConcreteFilter() {
        return new CardinalityFilter(minCardinality, maxCardinality);
    }
}
