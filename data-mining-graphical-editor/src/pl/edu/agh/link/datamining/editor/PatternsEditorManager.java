package pl.edu.agh.link.datamining.editor;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.edu.agh.link.datamining.editor.preview.filters.IPatternFilter;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class PatternsEditorManager<T extends DMEditor> {
    protected final T editor;

    private List<IPatternFilter> filters = new LinkedList<>();

    private Collection<IFrequentPattern> lastPatterns;

    public PatternsEditorManager(T previewEditor) {
        this.editor = previewEditor;

    }

    public void setPatterns(Collection<IFrequentPattern> patterns) {
        lastPatterns = patterns;
        refresh();
    }

    public Collection<IFrequentPattern> getFilteredPatterns() {
        return filterPatterns(lastPatterns);
    }

    public void setZoom(int zoom) {
        editor.setScale((double)(zoom) / 100);
    }

    public int getZoom() {
        return (int)(editor.getScale() * 100);
    }

    public void resetFilters() {
        filters.clear();
    }

    private Collection<IFrequentPattern> filterPatterns(Collection<IFrequentPattern> patterns) {
        if (filters.size() < 1) {
            return patterns;
        }
        Collection<IFrequentPattern> copy = new LinkedList<>();
        for (IFrequentPattern pattern : patterns) {
            copy.add(pattern);
        }
        for (IPatternFilter filter : filters) {
            copy = filter.filter(copy);
        }
        return copy;
    }

    public void refresh() {
        Collection<IFrequentPattern> filteredPatterns = filterPatterns(lastPatterns);
        Map<Integer, List<IFrequentPattern>> patternMap = new HashMap<>();
        for (IFrequentPattern pattern : filteredPatterns) {
            if (!patternMap.containsKey(Integer.valueOf(pattern.size()))) {
                patternMap.put(Integer.valueOf(pattern.size()), new LinkedList<IFrequentPattern>());
            }
            patternMap.get(Integer.valueOf(pattern.size())).add(pattern);

        }
        editor.setPatterns(patternMap);
        editor.repaintPatterns();
        editor.setScale(1);
    }

    public void addFilter(IPatternFilter filter) {
        filters.add(filter);
    }

}
