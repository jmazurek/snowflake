package pl.edu.agh.link.datamining.editor.figures.location;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Point;


public class CenterLocationStrategy implements IPatternLocationStrategy {

    private final Figure workspace;

    public CenterLocationStrategy(Figure workspace) {
        this.workspace = workspace;
    }

    @Override
    public Point computeLocation(Shape pattern) {
        return new Point((workspace.getSize().width - pattern.getSize().width)/2, (workspace.getSize().height - pattern.getSize().height)/2);
    }

}
