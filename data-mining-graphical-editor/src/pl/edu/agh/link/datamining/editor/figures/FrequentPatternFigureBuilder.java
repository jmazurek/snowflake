package pl.edu.agh.link.datamining.editor.figures;

import org.eclipse.draw2d.Shape;

import pl.edu.agh.link.datamining.editor.figures.coloring.IPatternColoringStrategy;
import pl.edu.agh.link.datamining.editor.figures.location.IPatternLocationStrategy;
import pl.edu.agh.link.datamining.editor.figures.presentation.IPatternPresentationStrategy;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class FrequentPatternFigureBuilder {
    private final IPatternPresentationStrategy patternStrategy;

    private final IPatternColoringStrategy coloringStrategy;

    private final IPatternLocationStrategy locationStrategy;

    public FrequentPatternFigureBuilder(IPatternPresentationStrategy patternStrategy, IPatternColoringStrategy coloringStrategy,
            IPatternLocationStrategy locationStrategy) {
        this.patternStrategy = patternStrategy;
        this.coloringStrategy = coloringStrategy;
        this.locationStrategy = locationStrategy;

    }

    public Shape build(IFrequentPattern pattern) {
        Shape figure = patternStrategy.buildFigure(pattern);
        figure.setBackgroundColor(coloringStrategy.getColor(pattern));
        figure.setLocation(locationStrategy.computeLocation(figure));
        return figure;
    }

}
