package pl.edu.agh.link.datamining.editor.figures.selection;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.draw2d.Shape;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class SingleSelectionStrategy implements ISelectionStrategy {

    private Shape figureSelected;

    private IFrequentPattern patternSelected;

    @Override
    public void patternSelected(IFrequentPattern pattern, Shape patternShape) {
        if (figureSelected != null) {
            figureSelected.setOutline(false);
        }
        figureSelected = patternShape;
        patternSelected = pattern;
        figureSelected.setOutline(true);

    }

    @Override
    public Collection<IFrequentPattern> getSelectedPatterns() {
        List<IFrequentPattern> retVal = new LinkedList<>();
        retVal.add(patternSelected);
        return retVal;
    }

    @Override
    public Collection<Shape> getSelectedShapes() {
        List<Shape> retVal = new LinkedList<>();
        retVal.add(figureSelected);
        return retVal;
    }

    @Override
    public void clearSelection() {
        figureSelected.setOutline(false);
        figureSelected = null;
        patternSelected = null;

    }

}
