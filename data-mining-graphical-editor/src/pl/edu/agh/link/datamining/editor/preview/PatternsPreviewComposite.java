package pl.edu.agh.link.datamining.editor.preview;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import pl.edu.agh.cast.ui.util.ColorUtil;

import pl.edu.agh.link.datamining.editor.preview.PreviewEditor.SizeListener;

public class PatternsPreviewComposite extends Composite {

    private final FigureCanvas workspace;

    private final PreviewEditor previewEditor;

    private final PreviewEditorManager manager;

    public PatternsPreviewComposite(Composite parent, int style) {
        super(parent, style);
        previewEditor = new PreviewEditor();
        manager = new PreviewEditorManager(previewEditor);
        workspace = new FigureCanvas(this);
        workspace.setContents(previewEditor.getEditorSpace());
        workspace.setSize(parent.getShell().getSize());
        workspace.setBackground(ColorUtil.getColor(255, 255, 255));
        previewEditor.addSizeListener(new SizeListener() {

            @Override
            public void sizeChanged(Point size) {
                int x = workspace.getSize().x, y = workspace.getSize().y;
                if (size.x > workspace.getSize().x) {
                    x = size.x;
                }
                if (size.y > workspace.getSize().y) {
                    y = size.y;
                }
                workspace.setSize(new org.eclipse.swt.graphics.Point(x, y));
            }
        });
        GridDataFactory.fillDefaults().hint(SWT.DEFAULT, SWT.DEFAULT).applyTo(this);
    }

    public PreviewEditorManager getManager() {
        return manager;
    }

}
