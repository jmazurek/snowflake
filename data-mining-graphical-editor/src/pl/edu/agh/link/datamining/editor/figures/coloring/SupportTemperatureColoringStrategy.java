package pl.edu.agh.link.datamining.editor.figures.coloring;

import org.eclipse.swt.graphics.Color;

import pl.edu.agh.link.datamining.editor.util.TemperatureColorCalculator;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class SupportTemperatureColoringStrategy implements
		IPatternColoringStrategy {

    private final int minSupport;
    private final int maxSupport;

	public SupportTemperatureColoringStrategy(int minSupport, int maxSupport) {
		this.minSupport = minSupport;
		this.maxSupport = maxSupport;
	}

	@Override
	public Color getColor(IFrequentPattern pattern) {
		return TemperatureColorCalculator.getTemperatureColor((double)(pattern.getAbsSupport()-minSupport)/(maxSupport-minSupport));
	}

}
