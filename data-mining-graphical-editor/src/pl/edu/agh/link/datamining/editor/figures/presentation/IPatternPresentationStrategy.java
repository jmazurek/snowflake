package pl.edu.agh.link.datamining.editor.figures.presentation;

import org.eclipse.draw2d.Shape;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public interface IPatternPresentationStrategy {
    Shape buildFigure(IFrequentPattern pattern);
}
