package pl.edu.agh.link.datamining.editor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPart;

import pl.edu.agh.cast.editor.CastEditor;
import pl.edu.agh.cast.ui.EditorID;

import pl.edu.agh.link.datamining.editor.preview.PatternsPreviewComposite;
import pl.edu.agh.link.datamining.editor.preview.PreviewEditorManager;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternsDataSet;

public class DataMiningEditor extends CastEditor<IFrequentPatternsDataSet> {

    static private final String ID = "pl.edu.agh.link.datamining.editor.DataMiningEditor"; //$NON-NLS-1$

    private List<NewDataListener> listeners = new LinkedList<>();

    private PatternsPreviewComposite previewEditorComposite;

    private PreviewEditorManager previewManager;

    @SuppressWarnings("rawtypes")
    @Override
    public Object getAdapter(Class adapter) {
        if (adapter == EditorID.class) {
            return new EditorID(ID);
        }
        return super.getAdapter(adapter);
    }

    @Override
    protected void setInput(IEditorInput input) {

        super.setInput(input);

        invokeNewDataListeners();
    }

    public IFrequentPatternsDataSet getDataSet() {
        return getModel();
    }

    public Collection<IFrequentPattern> getFilteredPatterns() {
        return previewManager.getFilteredPatterns();
    }

    @Override
    public void selectionChanged(IWorkbenchPart part, ISelection selection) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doSave(IProgressMonitor monitor) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doSaveAs() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isDirty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSaveAsAllowed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void createPartControl(Composite parent) {
        GridLayoutFactory.fillDefaults().numColumns(2).applyTo(parent);
        GridDataFactory.fillDefaults().hint(SWT.DEFAULT, SWT.DEFAULT).applyTo(parent);
        createPreviewEditorContainer(parent);
    }

    private void createPreviewEditorContainer(Composite parent) {
        previewEditorComposite = new PatternsPreviewComposite(parent, SWT.NONE);
        GridLayoutFactory.fillDefaults().applyTo(previewEditorComposite);
        previewManager = previewEditorComposite.getManager();
        previewManager.setPatterns(getModel().getFrequentPatterns());
    }

    public void addNewDataListener(NewDataListener listener) {
        listeners.add(listener);
    }

    private void invokeNewDataListeners() {
        for (NewDataListener listener : listeners) {
            listener.newData();
        }
    }

    @Override
    public void setFocus() {
        // TODO Auto-generated method stub

    }

    public PreviewEditorManager getPreviewEditorManager() {
        return previewManager;
    }

    public static interface NewDataListener {
        void newData();
    }

}
