package pl.edu.agh.link.datamining.editor.figures.selection;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.draw2d.Shape;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class MultiSelectionStrategy implements ISelectionStrategy {

    private Set<IFrequentPattern> selectedPatterns = new HashSet<>();

    private Set<Shape> selectedShapes = new HashSet<>();

    @Override
    public void patternSelected(IFrequentPattern pattern, Shape patternShape) {
        if (selectedPatterns.contains(pattern) && selectedShapes.contains(patternShape)) {
            patternShape.setOutline(false);
            selectedPatterns.remove(pattern);
            selectedShapes.remove(patternShape);
        } else if (!selectedPatterns.contains(pattern) && !selectedShapes.contains(patternShape)) {
            patternShape.setOutline(true);
            selectedPatterns.add(pattern);
            selectedShapes.add(patternShape);
        } else {
            throw new IllegalStateException("There is no pair Shape-IFrequentPattern in sets"); //$NON-NLS-1$
        }
    }

    @Override
    public Collection<IFrequentPattern> getSelectedPatterns() {
        return selectedPatterns;
    }

    @Override
    public Collection<Shape> getSelectedShapes() {
        return selectedShapes;
    }

    @Override
    public void clearSelection() {
        for (Shape shape : selectedShapes) {
            shape.setOutline(false);
        }
        selectedPatterns.clear();
        selectedShapes.clear();
    }

}
