package pl.edu.agh.link.datamining.editor.figures.selection;

import java.util.Collection;

import org.eclipse.draw2d.Shape;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public interface ISelectionStrategy {
    public void patternSelected(IFrequentPattern pattern, Shape patternShape);

    public Collection<IFrequentPattern> getSelectedPatterns();

    public Collection<Shape> getSelectedShapes();

    public void clearSelection();
}
