package pl.edu.agh.link.datamining.editor.toolkit;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import pl.edu.agh.link.datamining.editor.preview.filters.AbstractFilterFactory;
import pl.edu.agh.link.datamining.editor.preview.filters.SupportFilterFactory;
import pl.edu.agh.link.datamining.editor.util.Config;
import pl.edu.agh.link.datamining.editor.util.Messages;

public class SupportFilterWidget extends AbstractFilterWidget {

    private Spinner minSupp;

    private Spinner maxSupp;

    private Button enableCheckbox;

    private SupportFilterFactory factory;

    public SupportFilterWidget(Composite parent, int style) {
        super(parent, style);
        GridLayoutFactory.fillDefaults().numColumns(4).applyTo(this);

        createMinimumSupportSpinner();
        createMaximumSupportSpinner();
        createEnableCheckbox();

        factory = new SupportFilterFactory();

        Label label = new Label(this, SWT.NONE);
        label.setText(Messages.ToolkitView_Support);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (!enabled) {
            enableCheckbox.setSelection(false);
            minSupp.setSelection(0);
            maxSupp.setSelection(0);
        }
        enableCheckbox.setEnabled(enabled);
        minSupp.setEnabled(enableCheckbox.getSelection());
        maxSupp.setEnabled(enableCheckbox.getSelection());
        super.setEnabled(enabled);
    }

    private void createEnableCheckbox() {
        enableCheckbox = new Button(this, SWT.CHECK);
        enableCheckbox.setSelection(false);
        enableCheckbox.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                maxSupp.setEnabled(enableCheckbox.getSelection());
                minSupp.setEnabled(enableCheckbox.getSelection());
                factory.setEnabled(enableCheckbox.getSelection());
            }
        });
    }

    private void createMaximumSupportSpinner() {
        maxSupp = new Spinner(this, SWT.NONE);
        maxSupp.setMinimum(Config.MAX_SUPPORT_LEFT_BOUND);
        maxSupp.setMaximum(Config.MAX_SUPPORT_RIGHT_BOUND);
        maxSupp.setEnabled(false);
        maxSupp.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                factory.setMaxSupport(maxSupp.getSelection());
            }
        });
    }

    private void createMinimumSupportSpinner() {
        minSupp = new Spinner(this, SWT.NONE);
        minSupp.setMinimum(Config.MIN_SUPPORT_LEFT_BOUND);
        minSupp.setMaximum(Config.MIN_SUPPORT_RIGHT_BOUND);
        minSupp.setEnabled(false);
        minSupp.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                factory.setMinSupport(minSupp.getSelection());
            }
        });
    }

    @Override
    public AbstractFilterFactory getFilterFactory() {
        return factory;
    }

}
