package pl.edu.agh.link.datamining.editor.figures.location;

import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Point;

public class IncrementalLocationStrategy implements IPatternLocationStrategy {

    private int position = 0;

    private int currentGap = 0;

    private int lastSupport = 0;

    private final int scale;

    private boolean isSupportSet = false;

    public IncrementalLocationStrategy(int scale, int initialSupport) {
        this.scale = scale;
        lastSupport = initialSupport;
    }

    @Override
    public Point computeLocation(Shape pattern) {
        if (!isSupportSet) {
            throw new IllegalStateException("Set pattern support first!"); //$NON-NLS-1$
        }
        position += currentGap * pattern.getSize().width;
        Point location = new Point(position, 1);
        position += pattern.getSize().width;
        isSupportSet = false;
        return location;
    }

    public void setSupport(int support) {
        isSupportSet = true;
        currentGap = (lastSupport - support) * scale;
        lastSupport = support;
    }

    public int getPosition() {
        return position;
    }

}
