package pl.edu.agh.link.datamining.editor.preview.filters;

public abstract class AbstractFilterFactory {

    private boolean enabled = false;

    public final IPatternFilter buildFilter() {
        if (!enabled) {
            return new NullFilter();
        } else
            return buildConcreteFilter();
    }

    protected abstract IPatternFilter buildConcreteFilter();

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
