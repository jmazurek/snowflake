package pl.edu.agh.link.datamining.editor.toolkit;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import pl.edu.agh.cast.ui.util.PartListenerAdapter;

import pl.edu.agh.link.datamining.editor.DataMiningEditor;
import pl.edu.agh.link.datamining.editor.PatternsEditorManager;
import pl.edu.agh.link.datamining.editor.util.Config;
import pl.edu.agh.link.datamining.editor.util.Messages;

public class ToolkitView extends ViewPart {

    private DataMiningEditor editor = null;

    private PatternsEditorManager<?> manager = null;

    private List<AbstractFilterWidget> filterWidgets = new LinkedList<>();

    private Button applyButton;

    private Scale zoom;

    @Override
    public void createPartControl(Composite parent) {
        createMainBox(parent);
        initialize();
    }

    private void initialize() {
        IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
        System.out.println(page);
        if (page.getActiveEditor() instanceof DataMiningEditor) {
            DataMiningEditor editor = (DataMiningEditor)page.getActiveEditor();
            manager = editor.getPreviewEditorManager();
            zoom.setSelection(manager.getZoom());
        } else {
            setEnabled(false);
        }
        createPartListener();
        createScaleListener();
    }

    private void createScaleListener() {
        zoom.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                manager.setZoom(zoom.getSelection());
            }
        });

    }

    private Composite createMainBox(Composite parent) {
        Composite mainBox = new Composite(parent, SWT.NONE);
        GridLayoutFactory.fillDefaults().applyTo(mainBox);
        GridDataFactory.fillDefaults().applyTo(mainBox);
        createZoomSlider(mainBox);
        createFilters(mainBox);
        return mainBox;
    }

    private void setEnabled(boolean enabled) {
        for (Composite filterWidget : filterWidgets) {
            filterWidget.setEnabled(enabled);
        }
        applyButton.setEnabled(enabled);
    }

    private void createZoomSlider(Composite parent) {
        Group zoomBox = new Group(parent, SWT.NONE);
        zoomBox.setText(Messages.ToolkitView_ViewGroup);
        GridLayoutFactory.fillDefaults().numColumns(2).applyTo(zoomBox);
        GridDataFactory.fillDefaults().applyTo(parent);
        zoom = new Scale(zoomBox, SWT.None);
        GridDataFactory.fillDefaults().span(1, 2).applyTo(zoom);
        zoom.setMinimum(Config.ZOOM_MIN);
        zoom.setMaximum(Config.ZOOM_MAX);
        Label label = new Label(zoomBox, SWT.NONE);
        label.setText(Messages.ToolkitView_Zoom);
        final Label zoomValue = new Label(zoomBox, SWT.NONE);
        GridDataFactory.fillDefaults().minSize(50, 20).applyTo(zoomValue);
        zoom.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                zoomValue.setText(zoom.getSelection() + "%"); //$NON-NLS-1$
            }
        });

    }

    private void createFilters(Composite parent) {
        Group filterGroup = new Group(parent, SWT.NONE);
        filterGroup.setText(Messages.ToolkitView_FiltersGroup);
        GridLayoutFactory.fillDefaults().applyTo(filterGroup);
        GridDataFactory.fillDefaults().applyTo(filterGroup);

        filterWidgets.add(new CardinalityFilterWidget(filterGroup, SWT.NONE));
        filterWidgets.add(new SupportFilterWidget(filterGroup, SWT.NONE));
        filterWidgets.add(new NumberFilterWidget(filterGroup, SWT.NONE));

        createApplyButton(filterGroup);

    }

    private void createApplyButton(Composite parent) {
        applyButton = new Button(parent, SWT.NONE);
        applyButton.setText(Messages.ToolkitView_Apply);
        applyButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                manager.resetFilters();
                for (AbstractFilterWidget widget : filterWidgets) {
                    manager.addFilter(widget.getFilterFactory().buildFilter());
                }
                manager.refresh();
            }
        });
    }

    private void createPartListener() {
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().addPartListener(new PartListenerAdapter() {
            @Override
            public void partActivated(IWorkbenchPart part) {
                if (part instanceof DataMiningEditor) {
                    editor = (DataMiningEditor)part;
                    manager = editor.getPreviewEditorManager();
                    setEnabled(true);
                }

            }

            @Override
            public void partClosed(IWorkbenchPart part) {
                if (part instanceof ToolkitView) {
                    zoom = null;
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().removePartListener(this);
                } else if (part instanceof DataMiningEditor) {
                    editor = null;
                    manager = null;
                    setEnabled(false);
                }

            }

        });
    }

    @Override
    public void setFocus() {
        // TODO Auto-generated method stub

    }

}
