package pl.edu.agh.link.datamining.editor.preview.filters;

public class NumberFilterFactory extends AbstractFilterFactory {

    private String number;

    @Override
    protected IPatternFilter buildConcreteFilter() {
        return new NumberFilter(number);
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
