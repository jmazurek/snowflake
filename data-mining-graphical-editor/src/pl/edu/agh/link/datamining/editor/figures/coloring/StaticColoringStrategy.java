package pl.edu.agh.link.datamining.editor.figures.coloring;

import org.eclipse.swt.graphics.Color;

import pl.edu.agh.cast.ui.util.ColorUtil;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class StaticColoringStrategy implements IPatternColoringStrategy {

	private Color color = ColorUtil.getColor(0, 0, 0);

	public void setColor(Color color){
		this.color = color;
	}

	@Override
	public Color getColor(IFrequentPattern pattern) {
		return color;
	}

}
