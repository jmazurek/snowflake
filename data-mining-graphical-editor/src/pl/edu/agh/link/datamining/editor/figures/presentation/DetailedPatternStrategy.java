package pl.edu.agh.link.datamining.editor.figures.presentation;

import java.util.List;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import pl.edu.agh.cast.ui.util.ColorUtil;

import pl.edu.agh.link.datamining.editor.util.Config;
import pl.edu.agh.link.datamining.editor.util.NodePositionCalculator;
import pl.edu.agh.link.datamining.editor.util.PatternLabeler;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPatternItem;

public class DetailedPatternStrategy implements IPatternPresentationStrategy {

    private static final double _FULL_ANGLE = 360d;

    @Override
    public Shape buildFigure(IFrequentPattern pattern) {
        List<IFrequentPatternItem> items = pattern.getItemList();
        final Ellipse mainEllipse = new Ellipse();
        mainEllipse.setBounds(new Rectangle(0, 0, Config.DETAILED_PATTERN_CIRCLE_RADIUS, Config.DETAILED_PATTERN_CIRCLE_RADIUS));
        mainEllipse.setAlpha(100);
        mainEllipse.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseMoved(MouseEvent me) {
            }

            @Override
            public void mouseHover(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
                mainEllipse.setAlpha(100);
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                mainEllipse.setAlpha(255);
            }

            @Override
            public void mouseDragged(MouseEvent me) {
            }
        });
        double alfa = _FULL_ANGLE / items.size(), a = 180;
        Dimension dim = new Dimension(Config.DETAILED_PATTERN_NODE_RADIUS, Config.DETAILED_PATTERN_NODE_RADIUS);
        for (IFrequentPatternItem item : items) {
            final Ellipse ellipse = new Ellipse();
            ellipse.setBackgroundColor(ColorUtil.getColor(255, 255, 255));
            ellipse.setToolTip(new org.eclipse.draw2d.Label(item.toString()));
            Point pos = NodePositionCalculator.computePosition(Config.DETAILED_PATTERN_CIRCLE_RADIUS / 2 - 2
                    * Config.DETAILED_PATTERN_NODE_RADIUS, a);
            pos.x = pos.x + 3 * Config.DETAILED_PATTERN_NODE_RADIUS / 2;
            pos.y = pos.y + 3 * Config.DETAILED_PATTERN_NODE_RADIUS / 2;
            ellipse.setBounds(new Rectangle(pos, dim));
            mainEllipse.add(ellipse);
            a += alfa;
            ellipse.addMouseMotionListener(new MouseMotionListener() {

                @Override
                public void mouseMoved(MouseEvent me) {
                }

                @Override
                public void mouseHover(MouseEvent me) {
                }

                @Override
                public void mouseExited(MouseEvent me) {
                    ellipse.setBackgroundColor(ColorUtil.getColor(255, 255, 255));
                }

                @Override
                public void mouseEntered(MouseEvent me) {
                    ellipse.setBackgroundColor(ColorUtil.getColor(255, 255, 0));
                    mainEllipse.handleMouseEntered(me);
                }

                @Override
                public void mouseDragged(MouseEvent me) {
                }
            });
        }
        mainEllipse.setToolTip(new Label(PatternLabeler.createGroupLabel(pattern)));
        return mainEllipse;
    }

}
