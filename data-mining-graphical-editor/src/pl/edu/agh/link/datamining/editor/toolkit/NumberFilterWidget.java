package pl.edu.agh.link.datamining.editor.toolkit;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import pl.edu.agh.link.datamining.editor.preview.filters.AbstractFilterFactory;
import pl.edu.agh.link.datamining.editor.preview.filters.NumberFilterFactory;
import pl.edu.agh.link.datamining.editor.util.Messages;

public class NumberFilterWidget extends AbstractFilterWidget {

    private Text number;

    private Button enableCheckbox;

    private NumberFilterFactory factory;

    public NumberFilterWidget(Composite parent, int style) {
        super(parent, style);
        GridLayoutFactory.fillDefaults().numColumns(4).applyTo(this);

        createNumberTextField();
        createEnableCheckbox();

        factory = new NumberFilterFactory();

        Label label = new Label(this, SWT.NONE);
        label.setText(Messages.ToolkitView_Number);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (!enabled) {
            enableCheckbox.setSelection(false);
            number.setText(""); //$NON-NLS-1$
        }
        number.setEnabled(enableCheckbox.getSelection());
        enableCheckbox.setEnabled(enabled);
        super.setEnabled(enabled);
    }

    private void createEnableCheckbox() {
        enableCheckbox = new Button(this, SWT.CHECK);
        enableCheckbox.setSelection(false);
        enableCheckbox.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                number.setEnabled(enableCheckbox.getSelection());
                factory.setEnabled(enableCheckbox.getSelection());
            }
        });
    }

    private void createNumberTextField() {
        number = new Text(this, SWT.SINGLE);
        number.setEnabled(false);
        number.setSize(SWT.DEFAULT, 12);
        number.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {
                factory.setNumber(number.getText());
            }

            @Override
            public void focusGained(FocusEvent e) {
            }
        });
    }

    @Override
    public AbstractFilterFactory getFilterFactory() {
        return factory;
    }

}
