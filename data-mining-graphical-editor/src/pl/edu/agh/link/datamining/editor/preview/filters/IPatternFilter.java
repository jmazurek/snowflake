/*
 * This file is a part of CAST project.
 * (c) Copyright 2007, AGH University of Science & Technology
 * https://caribou.iisg.agh.edu.pl/trac/cast
 *
 * Licensed under the Eclipse Public License, Version 1.0 (the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.eclipse.org/legal/epl-v10.html
 */
/*
 * File: IPatternFilter.java
 * Created: 2014-12-04
 * Author: CAST-user
 * $Id$
 */

package pl.edu.agh.link.datamining.editor.preview.filters;

import java.util.Collection;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public interface IPatternFilter {
    Collection<IFrequentPattern> filter(Collection<IFrequentPattern> patterns);
}
