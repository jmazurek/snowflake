package pl.edu.agh.link.datamining.editor.util;

import org.eclipse.swt.graphics.Color;

import pl.edu.agh.cast.ui.util.ColorUtil;

public class TemperatureColorCalculator {
    private static final double alfa = 4;
    private static final double redP = 1;
    private static final double greenP = 0.5;
    private static final double blueP = 0;

    private static double mainFunction(double P, double x){
        double value = -alfa*(Math.abs(x-P))+2;
        if (value>1) value = 1;
        if (value<0) value = 0;
        return value;
    }

    public static Color getTemperatureColor(double percentage){
        int red = (int)Math.round(255*mainFunction(redP, percentage));
        int green = (int)Math.round(255*mainFunction(greenP, percentage));
        int blue = (int)Math.round(255*mainFunction(blueP, percentage));
        return ColorUtil.getColor(red, green, blue);
    }
}
