package pl.edu.agh.link.datamining.editor.util;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
    private static final String BUNDLE_NAME = "pl.edu.agh.link.datamining.editor.util.messages"; //$NON-NLS-1$

    public static String ToolkitView_Apply;

    public static String ToolkitView_Cardinality;

    public static String ToolkitView_FiltersGroup;

    public static String ToolkitView_Number;

    public static String ToolkitView_Support;

    public static String ToolkitView_ViewGroup;

    public static String ToolkitView_Zoom;

    public static String DataMiningElementsView_Both;

    public static String DataMiningElementsView_Color;

    public static String DataMiningElementsView_ElementCount;

    public static String DataMiningElementsView_FlatView;

    public static String DataMiningElementsView_Name;

    public static String DataMiningElementsView_Support;

    public static String DataMiningElementsView_TreeView;

    public static String DetailedView_NotSupportedDataType;

    public static String PatternLabeler_AbsoluteSupport;

    static {
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }

}
