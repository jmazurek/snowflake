package pl.edu.agh.link.datamining.editor.preview.figures;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Dimension;

import pl.edu.agh.link.datamining.editor.figures.FrequentPatternFigureBuilder;
import pl.edu.agh.link.datamining.editor.figures.coloring.IPatternColoringStrategy;
import pl.edu.agh.link.datamining.editor.figures.coloring.SupportTemperatureColoringStrategy;
import pl.edu.agh.link.datamining.editor.figures.location.IncrementalLocationStrategy;
import pl.edu.agh.link.datamining.editor.figures.presentation.IPatternPresentationStrategy;
import pl.edu.agh.link.datamining.editor.figures.presentation.PreviewPatternPresentationStrategy;
import pl.edu.agh.link.datamining.editor.preview.PreviewEditor;
import pl.edu.agh.link.datamining.editor.util.Config;
import pl.edu.agh.link.datamining.editor.util.PatternLabeler;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class PreviewPatternGroupFigureBuilder {
    private Dimension size = new Dimension(0, 0);

    private List<IFrequentPattern> patterns;

    private IPatternPresentationStrategy presentationStrategy;

    private IPatternColoringStrategy coloringStrategy;

    private IncrementalLocationStrategy locationStrategy;

    private PreviewEditor editor;

    public PreviewPatternGroupFigureBuilder(PreviewEditor editor) {
        this.editor = editor;
    }

    public PreviewPatternGroupFigureBuilder withSize(Dimension size) {
        this.size = size;
        return this;
    }

    public PreviewPatternGroupFigureBuilder withPatterns(List<IFrequentPattern> patterns) {
        this.patterns = patterns;
        return this;
    }

    public IFigure build() {
        Collections.sort(patterns, new PatternComparator());
        createColoringStrategy();
        presentationStrategy = new PreviewPatternPresentationStrategy(size.width, size.height);
        IFigure patternGroup = createPatternsGroup();
        addPatternsToGroup(patternGroup);
        return patternGroup;
    }

    IFigure createPatternsGroup() {
        IFigure patternGroup = new RectangleFigure();

        return patternGroup;
    }

    private void addPatternsToGroup(IFigure patternGroup) {
        locationStrategy = new IncrementalLocationStrategy(Config.PATTERN_GAP_SCALE, patterns.get(0).getAbsSupport());
        FrequentPatternFigureBuilder patternFigureBuilder = new FrequentPatternFigureBuilder(presentationStrategy, coloringStrategy,
                locationStrategy);
        for (final IFrequentPattern pattern : patterns) {
            locationStrategy.setSupport(pattern.getAbsSupport());
            final Shape patternFig = patternFigureBuilder.build(pattern);
            patternFig.addMouseListener(new MouseListener() {

                @Override
                public void mouseReleased(MouseEvent me) {
                    editor.setSelected(pattern, patternFig);
                }

                @Override
                public void mousePressed(MouseEvent me) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void mouseDoubleClicked(MouseEvent me) {
                    // TODO Auto-generated method stub
                }
            });

            patternGroup.add(patternFig);
            patternFig.setToolTip(new Label(PatternLabeler.createGroupLabel(pattern)));
        }
        patternGroup.setSize(new Dimension(locationStrategy.getPosition(), size.height));
    }

    private void createColoringStrategy() {
        int maxSupport = patterns.get(0).getAbsSupport(), minSupport = patterns.get(patterns.size() - 1).getAbsSupport();

        coloringStrategy = new SupportTemperatureColoringStrategy(minSupport, maxSupport);
    }

    private class PatternComparator implements Comparator<IFrequentPattern> {
        @Override
        public int compare(IFrequentPattern o1, IFrequentPattern o2) {
            return -o1.getAbsSupport().compareTo(o2.getAbsSupport());
        }
    }

}
