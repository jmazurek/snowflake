package pl.edu.agh.link.datamining.editor.preview;

import org.eclipse.swt.graphics.Color;

import pl.edu.agh.link.datamining.editor.DMEditor.SelectionListener;
import pl.edu.agh.link.datamining.editor.PatternsEditorManager;
import pl.edu.agh.link.datamining.editor.details.DetailedView;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class PreviewEditorManager extends PatternsEditorManager<PreviewEditor> {

    private DetailedView detailedView;

    public PreviewEditorManager(PreviewEditor previewEditor) {
        super(previewEditor);
        initializeSelectionListener();
    }

    private void initializeSelectionListener() {
        editor.addSelectionListener(new SelectionListener() {

            @Override
            public void selected(IFrequentPattern pattern, Color color) {
                if (detailedView != null) {
                    detailedView.showPattern(pattern, color);
                }
            }
        });
    }

    public void setDetailedView(DetailedView detailedView) {
        this.detailedView = detailedView;
    }

}
