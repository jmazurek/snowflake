package pl.edu.agh.link.datamining.editor.toolkit;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import pl.edu.agh.link.datamining.editor.preview.filters.AbstractFilterFactory;
import pl.edu.agh.link.datamining.editor.preview.filters.CardinalityFilterFactory;
import pl.edu.agh.link.datamining.editor.util.Config;
import pl.edu.agh.link.datamining.editor.util.Messages;

public class CardinalityFilterWidget extends AbstractFilterWidget {
    private Spinner minCard;

    private Spinner maxCard;

    private Button enableCheckbox;

    private CardinalityFilterFactory factory;

    public CardinalityFilterWidget(Composite parent, int style) {
        super(parent, style);
        GridLayoutFactory.fillDefaults().numColumns(4).applyTo(this);

        createMinimumCardinalitySpinner();
        createMaximumCardinalitySpinner();
        createEnableCheckbox();
        factory = new CardinalityFilterFactory();

        Label label = new Label(this, SWT.NONE);
        label.setText(Messages.ToolkitView_Cardinality);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (!enabled) {
            enableCheckbox.setSelection(false);
            minCard.setSelection(0);
            maxCard.setSelection(0);
        }
        enableCheckbox.setEnabled(enabled);
        minCard.setEnabled(enableCheckbox.getSelection());
        maxCard.setEnabled(enableCheckbox.getSelection());
        super.setEnabled(enabled);
    }

    private void createEnableCheckbox() {
        enableCheckbox = new Button(this, SWT.CHECK);
        enableCheckbox.setSelection(false);
        enableCheckbox.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                maxCard.setEnabled(enableCheckbox.getSelection());
                minCard.setEnabled(enableCheckbox.getSelection());
                factory.setEnabled(enableCheckbox.getSelection());
            }
        });
    }

    private void createMaximumCardinalitySpinner() {
        maxCard = new Spinner(this, SWT.NONE);
        maxCard.setMinimum(Config.MAX_CARDARDINALITY_LEFT_BOUND);
        maxCard.setMaximum(Config.MAX_CARDARDINALITY_RIGHT_BOUND);
        maxCard.setEnabled(false);
        maxCard.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                factory.setMaxCardinality(maxCard.getSelection());
            }
        });
    }

    private void createMinimumCardinalitySpinner() {
        minCard = new Spinner(this, SWT.NONE);
        minCard.setMinimum(Config.MIN_CARDARDINALITY_LEFT_BOUND);
        minCard.setMaximum(Config.MIN_CARDARDINALITY_RIGHT_BOUND);
        minCard.setEnabled(false);
        minCard.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                factory.setMinCardinality(minCard.getSelection());
            }
        });
    }

    @Override
    public AbstractFilterFactory getFilterFactory() {
        return factory;
    }

}
