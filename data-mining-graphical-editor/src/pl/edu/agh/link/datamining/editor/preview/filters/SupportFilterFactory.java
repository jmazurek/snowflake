package pl.edu.agh.link.datamining.editor.preview.filters;

public class SupportFilterFactory extends AbstractFilterFactory {
    private int minSupport = 1;

    private int maxSupport = 1;

    @Override
    protected IPatternFilter buildConcreteFilter() {
        return new SupportFilter(minSupport, maxSupport);
    }

    public void setMaxSupport(int maxSupport) {
        this.maxSupport = maxSupport;
    }

    public void setMinSupport(int minSupport) {
        this.minSupport = minSupport;
    }
}
