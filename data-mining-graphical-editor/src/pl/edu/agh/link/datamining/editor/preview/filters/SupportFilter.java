/*
 * This file is a part of CAST project.
 * (c) Copyright 2007, AGH University of Science & Technology
 * https://caribou.iisg.agh.edu.pl/trac/cast
 *
 * Licensed under the Eclipse Public License, Version 1.0 (the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.eclipse.org/legal/epl-v10.html
 */
/*
 * File: SupportFilter.java
 * Created: 2014-12-04
 * Author: CAST-user
 * $Id$
 */

package pl.edu.agh.link.datamining.editor.preview.filters;

import java.util.Collection;
import java.util.Iterator;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

/**
 *
 * @author AGH CAST Team
 */
public class SupportFilter implements IPatternFilter {

    private final int min,max;

    public SupportFilter(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Collection<IFrequentPattern> filter(Collection<IFrequentPattern> patterns) {
        Iterator<IFrequentPattern> iterator = patterns.iterator();
        while(iterator.hasNext()){
            IFrequentPattern pattern = iterator.next();
            if(pattern.getAbsSupport()<min || pattern.getAbsSupport()>max){
                iterator.remove();
            }
        }
        return patterns;
    }

}
