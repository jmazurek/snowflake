package pl.edu.agh.link.datamining.editor;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ScalableFreeformLayeredPane;
import org.eclipse.draw2d.Shape;
import org.eclipse.swt.graphics.Color;

import pl.edu.agh.link.datamining.editor.figures.selection.ISelectionStrategy;
import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public abstract class DMEditor {

    protected final ScalableFreeformLayeredPane editorSpace;

    private final ISelectionStrategy selectionStrategy;

    private List<SelectionListener> selectionListeners = new LinkedList<>();

    protected Map<Integer, List<IFrequentPattern>> patterns;

    public DMEditor() {
        editorSpace = new ScalableFreeformLayeredPane();
        selectionStrategy = createSelectionStrategy();
    }

    protected abstract ISelectionStrategy createSelectionStrategy();

    public void setPatterns(Map<Integer, List<IFrequentPattern>> patternMap) {
        this.patterns = patternMap;
    }

    public abstract void repaintPatterns();

    public void setScale(double x) {
        editorSpace.setScale(x);
    }

    public double getScale() {
        return editorSpace.getScale();
    }

    public void addSelectionListener(SelectionListener listener) {
        selectionListeners.add(listener);
    }

    public IFigure getEditorSpace() {
        return editorSpace;
    }

    private void invokeSelectionListeners(IFrequentPattern pattern, Color color) {
        for (SelectionListener listener : selectionListeners) {
            listener.selected(pattern, color);
        }
    }

    public void setSelected(IFrequentPattern pattern, Shape figure) {
        selectionStrategy.patternSelected(pattern, figure);
        invokeSelectionListeners(pattern, figure.getBackgroundColor());
    }

    public static interface SelectionListener {
        public void selected(IFrequentPattern pattern, Color color);
    }

}
