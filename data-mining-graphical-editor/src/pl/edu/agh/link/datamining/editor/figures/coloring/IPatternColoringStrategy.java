package pl.edu.agh.link.datamining.editor.figures.coloring;

import org.eclipse.swt.graphics.Color;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public interface IPatternColoringStrategy {
	Color getColor(IFrequentPattern pattern);
}
