package pl.edu.agh.link.datamining.editor.preview.filters;

import java.util.Collection;

import pl.edu.agh.link.datamining.model.frequentpatterns.IFrequentPattern;

public class NullFilter implements IPatternFilter {

    @Override
    public Collection<IFrequentPattern> filter(Collection<IFrequentPattern> patterns) {
        return patterns;
    }

}
