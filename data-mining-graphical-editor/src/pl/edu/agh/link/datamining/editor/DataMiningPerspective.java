package pl.edu.agh.link.datamining.editor;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IViewLayout;

import pl.edu.agh.cast.ui.PerspectiveConstants;

public class DataMiningPerspective implements IPerspectiveFactory {

    public static final String ID = "pl.edu.agh.link.datamining.perspective"; //$NON-NLS-1$

    public static final String ELEMETS_VIEW_ID = "pl.edu.agh.link.datamining.elements.view.DataMiningElementsView"; //$NON-NLS-1$

    public static final String TOOLKIT_ID = "pl.edu.agh.link.datamining.view.DataMiningToolkit"; //$NON-NLS-1$

    public static final String DETAILS_ID = "pl.edu.agh.link.datamining.view.DataMiningDetails"; //$NON-NLS-1$

    public static final String RIGHT_FOLDER_ID = "pl.edu.agh.cast.ui.folder.right"; //$NON-NLS-1$

    @Override
    public void createInitialLayout(IPageLayout layout) {
        String editorArea = layout.getEditorArea();

        // top left
        IFolderLayout folderLayout = layout.createFolder(PerspectiveConstants.LEFT_TOP_FOLDER_ID, IPageLayout.LEFT, 0.2f, editorArea);
        folderLayout.addView(PerspectiveConstants.NAVIGATOR_VIEW_ID);
        IViewLayout viewLayout = layout.getViewLayout(PerspectiveConstants.NAVIGATOR_VIEW_ID);
        viewLayout.setCloseable(false);

        folderLayout = layout.createFolder(PerspectiveConstants.LEFT_BOTTOM_FOLDER_ID, IPageLayout.BOTTOM, 0.8f,
                PerspectiveConstants.LEFT_TOP_FOLDER_ID);
        folderLayout.addView(DETAILS_ID);

        // right side
        folderLayout = layout.createFolder(RIGHT_FOLDER_ID, IPageLayout.RIGHT, 0.8f, editorArea);
        folderLayout.addView(TOOLKIT_ID);

        // bottom
        folderLayout = layout.createFolder(PerspectiveConstants.BOTTOM_FOLDER_ID, IPageLayout.BOTTOM, 0.8f, editorArea);
        folderLayout.addView(ELEMETS_VIEW_ID);

    }

}
