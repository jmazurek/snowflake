package pl.edu.agh.link.datamining.editor.util;

public class Config {
    public static final int ZOOM_MIN = 50;

    public static final int ZOOM_MAX = 300;

    public static final int MIN_CARDARDINALITY_LEFT_BOUND = 1;

    public static final int MIN_CARDARDINALITY_RIGHT_BOUND = 20;

    public static final int MAX_CARDARDINALITY_LEFT_BOUND = 1;

    public static final int MAX_CARDARDINALITY_RIGHT_BOUND = 20;

    public static final int MIN_SUPPORT_LEFT_BOUND = 1;

    public static final int MIN_SUPPORT_RIGHT_BOUND = 100;

    public static final int MAX_SUPPORT_LEFT_BOUND = 1;

    public static final int MAX_SUPPORT_RIGHT_BOUND = 100;

    public static final int PATTERN_GAP_SCALE = 1;

    public static final int PREVIEW_PATTERN_WIDTH = 5;

    public static final int PREVIEW_PATTERN_HEIGHT = 40;

    public static final int PREVIEW_PATTERN_Y_OFFSET = 10;

    public static final int PREVIEW_PATTERN_BOTTOM_MARGIN = 50;

    public static final int PREVIEW_PATTERN_RIGHT_MARGIN = 50;

    public static final int PREVIEW_PATTERN_LEFT_MARGIN = 10;

    public static final int PREVIEW_PATTERN_TOP_MARGIN = 20;

    public static final int DETAILED_PATTERN_CIRCLE_RADIUS = 50;

    public static final int DETAILED_PATTERN_NODE_RADIUS = 9;

}
